import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class SharedService {
readonly APIUrl="https://test.arco.sa:92//api";
readonly PhotoUrl="https://test.arco.sa:92/Photos/";
  constructor(private http:HttpClient) { }
  getDepList():Observable<any[]>{
    return this.http.get<any>(this.APIUrl+'/TestDepartment');
  }
  
  addDepartment(val:any){
    return this.http.post(this.APIUrl+'/TestDepartment',val)
  }
  updateDepartment(val:any){
    return this.http.put(this.APIUrl+'/TestDepartment',val)
  }
  deleteDepartment(val:any){
    return this.http.delete(this.APIUrl+'/TestDepartment/'+val);
  }
  getEmpList():Observable<any[]>{
    return this.http.get<any>(this.APIUrl+'/TestEmployee');
  }
  addEmployee(val:any){
    return this.http.post(this.APIUrl+'/TestEmployee',val);
  }
  updateEmployee(val:any){
    return this.http.put(this.APIUrl+'/TestEmployee',val);
  }
  deleteEmployee(val:any){
    return this.http.delete(this.APIUrl+'/TestEmployee/'+val);
  }
  UploadPhoto(val:any){
    return this.http.post(this.APIUrl+'/TestEmployee/SaveFile/',val);
  }
  getAllDepartmentNames():Observable<any[]>{
    return this.http.get<any[]>(this.APIUrl+'/TestEmployee/GetAllDepartmentNames');
  }
// getphoto(){
//   return this.http.get(this.PhotoUrl);
// }
// *********************** LOCAL Storage**********************

  // getDepListByLocalStorage(){
  //   let table=[];
  //   let depdata = JSON.parse(localStorage.getItem('value'))
  //   depdata=table;
  //   table.push('value');
  //   let tabledata = JSON.stringify(table);
  //   localStorage.setItem('tabledata',tabledata);
  // }


}
