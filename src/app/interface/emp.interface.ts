export interface Emp{
        EmployeeId:number,
        EmployeeName:string,
        Department:string,
        DateOfJoining:string,
        PhotoFileName:string
}

export interface EmpList{
        EmployeeId:number,
        EmployeeName:string,
        Department:string,
        DateOfJoining:string,
        PhotoFileName:string
}