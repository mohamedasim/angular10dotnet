import { Component, OnInit } from '@angular/core';
import { SharedService} from 'src/app/shared.service';
import { Emp } from 'src/app/interface/emp.interface';
@Component({
  selector: 'app-show-emp',
  templateUrl: './show-emp.component.html',
  styleUrls: ['./show-emp.component.css']
})
export class ShowEmpComponent implements OnInit {
  constructor(private service:SharedService) { }
  EmployeeList:any=[];
  ModalTitle:string;
  ActivateAddEditEmpComp:boolean=false;
  emp:Emp={EmployeeId:0,EmployeeName:"",Department:"",DateOfJoining:"",PhotoFileName:""};
    ngOnInit(): void {
      this.emplist();
    }
  
    addClick(){debugger
      this.emp;
      this.ModalTitle="Add Employee";
      this.ActivateAddEditEmpComp=true;
    }
    editClick(item){
      console.log(item);
      this.emp=item;
      this.ModalTitle="Edit Employee";
      this.ActivateAddEditEmpComp=true;
    }
    deleteClick(item){
      if(confirm('Are you sure?')){
        this.service.deleteEmployee(item.EmployeeId).subscribe(data=>{
          alert(data.toString());
          this.emplist();
        });
      }
    }
    closeClick(){
      this.ActivateAddEditEmpComp=false;
      this.emplist();
    }
  emplist(){
    this.service.getEmpList().subscribe(data=>{
      this.EmployeeList=data;
    });
  }

}
