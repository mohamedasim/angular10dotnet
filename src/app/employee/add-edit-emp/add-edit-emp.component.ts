import { Component, Input, OnInit } from '@angular/core';
import { SharedService } from 'src/app/shared.service';
@Component({
  selector: 'app-add-edit-emp',
  templateUrl: './add-edit-emp.component.html',
  styleUrls: ['./add-edit-emp.component.css']
})
export class AddEditEmpComponent implements OnInit {
  constructor(private service:SharedService) { }
@Input()emp:any;
EmployeeId:string;
EmployeeName:string;
Department:string;
DateOfJoining:string;
PhotoFileName:string;
//PhotoFilePath:any;
url:any;
url1:any;
url2:any=null;
DepartmentsList:any=[];
  ngOnInit(): void {
    this.loadDepartmentList();
  }
  loadDepartmentList(){
    this.service.getAllDepartmentNames().subscribe((data:any)=>{
      this.DepartmentsList=data;
      this.EmployeeId=this.emp.EmployeeId;
      this.EmployeeName=this.emp.EmployeeName;
      this.Department=this.emp.Department;
      this.DateOfJoining=this.emp.DateOfJoining;
      this.PhotoFileName=this.emp.PhotoFileName;
     this.url=this.service.PhotoUrl+this.PhotoFileName;
     //this.url1=this.PhotoFileName
    });
  }
addEmployee(){debugger;
var val={
  EmployeeId:this.EmployeeId,
  EmployeeName:this.EmployeeName,
  Department:this.Department,
  DateOfJoining:this.DateOfJoining,
  PhotoFileName:this.PhotoFilePath.name
};
this.service.addEmployee(val).subscribe(res=>{
  this.uploadPhotoname(this.PhotoFilePath);
  alert(res.toString());
});

}
updateEmployee(){debugger;
  var val={
    EmployeeId:this.EmployeeId,
    EmployeeName:this.EmployeeName,
    Department:this.Department,
    DateOfJoining:this.DateOfJoining,
    PhotoFileName:this.PhotoFilePath.name
  };
  this.service.updateEmployee(val).subscribe(res=>{
    this.uploadPhotoname(this.PhotoFilePath);
    alert(res.toString());
  });
}
uploadPhotoname(file){
debugger
  const formData:FormData=new FormData();
  formData.append('uploadedFile',file,file.name);
   this.service.UploadPhoto(formData).subscribe((data:any)=>{
   
   });
}
PhotoFilePath:File=null;
uploadPhoto(files:FileList){debugger
  this.PhotoFilePath=files.item(0);
}


onSelectFile(event) {
  if (event.target.files && event.target.files[0]) {
    var reader = new FileReader();

    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (event) => { 
      this.url2 = event.target.result;
    }
  }
}
}
